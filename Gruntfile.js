/**
 * Created by broncha on 11/13/15.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            react: {
                files: 'frontend-src/react_components/**/*.jsx',
                tasks: ['browserify']
            },
            grunt: { files: ['Gruntfile.js'] },

            sass: {
                files: 'frontend-src/scss/**/*.scss',
                tasks: ['sass:dist'],
                options: {
                    livereload: true
                }
            }
        },

        browserify: {
            options: {
                transform: [ ['babelify', {presets: 'react'}] ]
            },
            dist: {
                src: ['frontend-src/react_components/**/*.jsx'],
                dest: 'frontend/dist/js/app.bundle.js'
            }
        },
        sass: {
            dist: {
                files: {
                    'frontend/dist/css/app.css': 'frontend-src/scss/moxie-main.scss'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');

    grunt.registerTask('default', [
        'sass','browserify', 'watch'
    ]);
};