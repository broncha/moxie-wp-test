<?php
defined( 'ABSPATH' ) or die( 'No direct access allowed.' );
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Moxie WP Test</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="<?php echo plugin_dir_url(__FILE__)."dist/css/animate.css" ?>" rel="stylesheet">
    <link href="<?php echo plugin_dir_url(__FILE__)."dist/css/app.css" ?>" rel="stylesheet">

    <script type="text/javascript">
        window.MOXIE = {
            'json_endpoint': '<?php echo site_url("api/movies")?>'
        };
    </script>
</head>
<body>
    <div class="container">
        <h1 class="main-title">Moxie WP Test</h1>

        <div id="moxie_container"></div>
    </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
<script src="<?php echo plugin_dir_url(__FILE__)."dist/js/app.bundle.js" ?>" type="text/javascript"></script>
</body>
</html>