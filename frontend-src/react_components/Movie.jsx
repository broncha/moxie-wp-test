var React = require('React');

var Movie = React.createClass({
    render: function() {
        return (
            <div className="col-sm-12 col-md-6 animated zoomIn">
                <div className="movie">
                    <div className="thumbnail">
                        <img src={this.props.data.poster_url} className="img-responsive" />
                        <div className="caption">
                            <h3>{this.props.data.title}</h3>
                            <p>
                                <strong>Year:</strong> <span className="year">{this.props.data.year}</span>
                                <strong>Rating:</strong> <span className="rating">{this.props.data.rating}</span>
                            </p>
                            <p>{this.props.data.short_description}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Movie;