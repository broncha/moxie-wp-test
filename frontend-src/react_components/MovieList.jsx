var React = require('React');
var Movie = require('./Movie.jsx');

var MovieList = React.createClass({
    render: function() {
        if (this.props.data.length == 0) {
            return this.renderNoMovies()
        }

        var movies = this.props.data.map(function(movie){
            return (
                <Movie data={movie} key={movie.id} />
            )
        });

        return (
            <div className="movies-list row">
            {movies}
            </div>
        );
    },
    renderNoMovies: function() {
        return (
            <div className="empty-list">There are no movies to show.</div>
        );
    }
});

module.exports = MovieList;