var React = require('React');
var MovieList = require('./MovieList.jsx');

var Container = React.createClass({
    componentDidMount: function(){
        var endpoint = this.props.endpoint;
        $.ajax({
            url: endpoint,
            dataType: "json",
            success: function(data){
                this.setState({data: data})
            }.bind(this),
            error: function(){

            }.bind(this)
        });
    },
    getInitialState: function(){
        return { data: [] }
    },
    render: function() {
        return (
            <MovieList data={this.state.data}/>
        );
    }
});

module.exports = Container;