var React = require('React');
var ReactDOM = require('react-dom');
var Container = require('./Container.jsx');

window.moxieContainer = ReactDOM.render(
    <Container endpoint={window.MOXIE.json_endpoint} />,
    document.getElementById('moxie_container')
);