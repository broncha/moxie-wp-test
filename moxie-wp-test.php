<?php
/**
 * Created by PhpStorm.
 * User: broncha
 * Date: 11/13/15
 * Time: 3:31 AM
 */

/*
Plugin Name: Moxie WP Test
Plugin URI: https://bitbucket.org/broncha/moxie-wp-test
Description: Test project for Moxie
Author: Rajesh Sharma
Version: 1.0
Author URI: http://rajesharma.com
*/

defined( 'ABSPATH' ) or die( 'No direct access allowed.' );

require_once dirname( __FILE__ ) . '/lib/class-tgm-plugin-activation.php';
require_once dirname( __FILE__ ) . '/includes/class-moxie-wp-test.php';
require_once dirname( __FILE__ ) . '/includes/class-json-api-manager.php';
require_once dirname( __FILE__ ) . '/includes/class-movie-manager.php';

$movie_manager = new Movie_Manager();
$json_api_manager = new JSON_Api_Manager($movie_manager);

$moxie_wp_test_plugin = new Moxie_WP_Test( $json_api_manager, $movie_manager );
$moxie_wp_test_plugin->init();