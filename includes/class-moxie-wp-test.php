<?php
/**
 * Created by PhpStorm.
 * User: broncha
 * Date: 11/13/15
 * Time: 11:07 PM
 */
defined( 'ABSPATH' ) or die( 'No direct access allowed.' );
class Moxie_WP_Test {
    /**
     * @var Movie_Manager
     */
    private $movie_manager;

    /**
     * @var JSON_Api_Manager
     */
    private $json_api_manager;

    /**
     * @param $json_api_manager
     * @param movie_manager
     */
    function __construct($json_api_manager, $movie_manager)
    {
        $this->json_api_manager = $json_api_manager;
        $this->movie_manager = $movie_manager;
    }

    public function init() {
        $this->init_required_plugins();
        $this->movie_manager->register();
        $this->json_api_manager->init();

        add_filter( 'template_include', function($template){
            if ( is_front_page() ) {
                return plugin_dir_path( __FILE__ ) . '../frontend/home-template.php';
            }

            return $template;
        }, 99 );
    }

    private function init_required_plugins() {
        add_action( 'tgmpa_register', function(){
            $plugins = array(
                array(
                    'name'               => 'Meta Box',
                    'slug'               => 'meta-box',
                    'required'           => true,
                    'force_activation'   => false,
                    'force_deactivation' => false,
                )
            );

            $config = array();
            tgmpa( $plugins, $config );
        } );
    }
}