<?php
/**
 * Created by PhpStorm.
 * User: broncha
 * Date: 11/13/15
 * Time: 9:29 PM
 */
defined( 'ABSPATH' ) or die( 'No direct access allowed.' );
class Movie_Manager{
    const POST_TYPE = "movie";
    const META_POSTER = "movie_meta_poster";
    const META_RATING = "movie_meta_rating";
    const META_YEAR = "movie_meta_year";
    const META_SHORT_DESCRIPTION = "movie_meta_description";

    const TRANSIENT_CACHE_KEY = "moxie_movies_json_cache_key";

    public function __construct() {

    }

    public function register() {
        add_action("init", array($this, "register_post_type"));
        add_action("rwmb_meta_boxes", array($this, "register_meta_boxes"));

        add_action("save_post", array($this, "post_save_movie"), 10, 3);

        add_filter('gettext',function($input){
            global $post_type;

            if( is_admin() && 'Enter title here' == $input ) {
                if(self::POST_TYPE == $post_type){
                    return "Enter movie's title here";
                }
            }

            return $input;
        });
    }

    public function post_save_movie($post_id, $post)
    {
        if ( $post->post_type != self::POST_TYPE ) {
            return;
        }

        if ( wp_is_post_revision( $post_id ) )
            return;

        delete_transient(self::TRANSIENT_CACHE_KEY);
    }

    public function register_post_type() {
        $labels = array(
            'name'               => _x( 'Movie', 'movie', 'nahhaft' ),
            'singular_name'      => _x( 'Movie', 'movie', 'nahhaft' ),
            'menu_name'          => _x( 'Movie', 'movie', 'nahhaft' ),
            'name_admin_bar'     => _x( 'Movie', 'movie', 'nahhaft' ),
            'add_new'            => _x( 'Add New', 'movie', 'nahhaft' ),
            'add_new_item'       => __( 'Add New Movie', 'nahhaft' ),
            'new_item'           => __( 'New Movie', 'nahhaft' ),
            'edit_item'          => __( 'Edit Movie', 'nahhaft' ),
            'view_item'          => __( 'View Movie', 'nahhaft' ),
            'all_items'          => __( 'All Movie', 'nahhaft' ),
            'search_items'       => __( 'Search Movie', 'nahhaft' ),
            'parent_item_colon'  => __( 'Parent Movie:', 'nahhaft' ),
            'not_found'          => __( 'No movie found.', 'nahhaft' ),
            'not_found_in_trash' => __( 'No movie found in Trash.', 'nahhaft' )
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => self::POST_TYPE ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title' )
        );

        register_post_type( self::POST_TYPE , $args );
    }

    public function register_meta_boxes($meta_boxes) {
        $meta_boxes[] = array(
            'id'    =>  "movie_post_type_meta_boxes",
            'title' =>  "Details",
            'post_types'    =>  array( self::POST_TYPE ),
            'context'       =>  "normal",
            "priority"      =>  "low",
            'fields'        =>  array(
                array(
                    "name"  =>  __("Rating", "nahhaft"),
                    "id"    =>  self::META_RATING,
                    "type"  =>  "text"
                ),
                array(
                    "name"  =>  __("Year", "nahhaft"),
                    "id"    =>  self::META_YEAR,
                    "type"  =>  "text"
                ),
                array(
                    'name'             => __( 'Movie poster', 'nahhaft' ),
                    'id'               => self::META_POSTER,
                    'type'             => 'image_advanced',
                    'desc'              =>  __("This image will be shown in the homepage as thumbnail"),
                    'max_file_uploads' => 1,
                ),
                array(
                    'name'  => __( 'Short Description', 'nahhaft' ),
                    'id'    => self::META_SHORT_DESCRIPTION,
                    'type'  => 'textarea',
                    'cols'  =>  60,
                    'rows'  =>  10
                )
            )
        );

        return $meta_boxes;
    }

    public function get_movies($perpage = 50) {
        $args = array(
            'post_type'      => self::POST_TYPE,
            'posts_per_page' => $perpage,
        );

        $query = new WP_Query($args);

        $movies = get_transient(self::TRANSIENT_CACHE_KEY);

        if($movies !== false) {
            return $movies;
        }

        $movies = array();

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {

                $query->the_post();

                $posterUrl = wp_get_attachment_image_src(get_post_meta(get_the_ID(), self::META_POSTER, true), "full");

                $movies[] = array(
                    'id'    =>  get_the_ID(),
                    'title' =>  get_the_title(),
                    'poster_url'    =>  $posterUrl[0],
                    'rating'    =>  get_post_meta(get_the_ID(), self::META_RATING, true),
                    'year'    =>  get_post_meta(get_the_ID(), self::META_YEAR, true),
                    'short_description'    =>  get_post_meta(get_the_ID(), self::META_SHORT_DESCRIPTION, true)
                );
            }
            wp_reset_postdata();
        }

        set_transient(self::TRANSIENT_CACHE_KEY, $movies, 12 * HOUR_IN_SECONDS);
        return $movies;
    }
}