<?php
/**
 * Created by PhpStorm.
 * User: broncha
 * Date: 11/13/15
 * Time: 11:11 PM
 */
defined( 'ABSPATH' ) or die( 'No direct access allowed.' );
class JSON_Api_Manager {

    const QUERY_VAR = "moxie_json_api";

    /**
     * @var Movie_Manager
     */
    private $movie_manager;

    /**
     * @param $movie_manager
     */
    function __construct($movie_manager)
    {
        $this->movie_manager = $movie_manager;
    }

    public function init() {

        add_filter( 'query_vars', function ( $vars ) {
            array_push( $vars, 'moxie_json_api' );
            return $vars;
        }, 1 );

        add_action("init", function() {
            add_rewrite_rule('^api/movies$', 'index.php?'.self::QUERY_VAR.'=movies', "top");
        });

        add_action( 'template_redirect', array($this, 'intercept') );
    }

    public function intercept() {
        $json_cpt = get_query_var( self::QUERY_VAR );

        if ( empty($json_cpt) || $json_cpt !== "movies" ) {
            return;
        }

        $movies = $this->movie_manager->get_movies();

        wp_send_json( $movies );
    }
}